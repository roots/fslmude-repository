# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="server operation"
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+virt"

RDEPEND="
	app-admin/eclean-kernel
	app-admin/mcelog
	app-admin/makepasswd
	dev-libs/libxml2[python]
	net-analyzer/iptraf-ng
	net-analyzer/munin[ipmi,minimal]
	net-analyzer/nagios-plugins[snmp]
	net-analyzer/nethogs
	net-misc/bridge-utils
	net-misc/ifenslave
	net-misc/ntp
	sys-apps/ethtool
	sys-apps/gptfdisk
	sys-apps/ipmitool
	sys-apps/lm-sensors
	sys-apps/lshw
	sys-apps/smartmontools
	sys-apps/usbutils
	sys-block/mpt-status
	sys-boot/grub
	sys-cluster/ceph[xfs]
	sys-fs/cryptsetup
	sys-fs/ddrescue
	sys-fs/dosfstools
	sys-fs/lvm2[thin]
	sys-fs/mdadm
	sys-fs/multipath-tools
	sys-fs/ntfs3g
	sys-fs/vmfs-tools
	sys-kernel/gentoo-sources
	sys-kernel/linux-firmware
	sys-power/apcupsd
	sys-power/cpupower
	virtual/fslmude-base[tsm]
	amd64? (
		sys-apps/edac-utils
	)
	virt? (
		app-emulation/libvirt[lvm,numa,virt-network,sasl,pcap,rbd]
		app-emulation/qemu[-jpeg,-png,vnc,numa,virtfs,xfs,gnutls,rbd]
		dev-python/libvirt-python
		amd64? (
			app-emulation/qemu[spice]
		)
	)
"
