# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Contains standard software to be installed on all systems"
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+tsm"

RDEPEND="
	app-admin/logrotate
	app-admin/makepasswd
	app-admin/needrestart
	app-admin/syslog-ng
	app-admin/sysstat
	app-admin/tmpreaper
	app-arch/hardlink
	app-arch/lzop
	tsm? ( app-backup/tsm )
	app-editors/vim
	app-misc/screen
	net-analyzer/nettop
	net-analyzer/nmap
	app-portage/eix
	app-portage/genlop
	app-portage/gentoolkit
	app-portage/layman
	app-portage/pfl
	app-portage/porticron
	app-portage/smart-live-rebuild
	app-shells/zsh
	app-vim/dirdiff
	dev-util/debootstrap
	dev-util/strace
	dev-vcs/git
	mail-client/mailx
	mail-mta/postfix
	net-analyzer/icinga2[minimal,-mysql,plugins]
	net-analyzer/iftop
	net-analyzer/munin[minimal]
	net-analyzer/nagios-plugins[ssh]
	net-analyzer/nmap
	net-analyzer/tcpdump
	net-dns/bind-tools
	net-misc/curl
	net-misc/dhcp
	net-misc/telnet-bsd
	sys-apps/iproute2
	sys-apps/kmod[lzma]
	sys-apps/mlocate
	sys-apps/pv
	sys-fs/ncdu
	sys-fs/xfsdump
	sys-fs/xfsprogs
	sys-power/acpid
	sys-process/atop
	sys-process/dcron
	sys-process/glances
	sys-process/htop
	sys-process/iotop
	sys-process/lsof
	sys-process/parallel
	www-client/links
	www-client/w3m
"
