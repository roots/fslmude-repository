# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="GAF desktop environment, forked from Michael Weber (xmw) desktop environment"
SRC_URI=""

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="minimal webmail"

RDEPEND="
	app-admin/conky[hddtemp,mpd,thinkpad,weather-metar,wifi]
	app-misc/hddled[X]
	app-shells/zsh
	app-text/mupdf
	app-text/paps
	app-text/zathura-cb
	app-text/zathura-djvu
	app-text/zathura-ps
	media-fonts/dina
	media-fonts/terminus-font
	media-gfx/feh
	media-gfx/scrot
	virtual/fslmuce
	x11-apps/setxkbmap
	x11-apps/xev
	x11-apps/xkill
	x11-apps/xmodmap
	x11-apps/xrandr
	x11-apps/xset
	x11-base/xorg-server
	x11-misc/alock
	x11-misc/dmenu
	x11-misc/rofi[windowmode]
	x11-misc/trayer-srg
	x11-misc/wmname
	x11-misc/x2x
	x11-misc/xbindkeys
	x11-misc/xclip
	x11-misc/xdotool
	x11-misc/xscreensaver
	x11-misc/xsel
	x11-terms/rxvt-unicode[xft]
	x11-wm/cwm
	media-fonts/font-misc-misc
	!minimal? (
		app-text/gv
		media-gfx/gimp
		media-sound/audacity
		media-sound/baudline
		media-video/mpv
		sci-astronomy/stellarium
		sci-electronics/geda
		sci-electronics/osqoop
		|| ( www-client/firefox-bin www-client/firefox )
		|| (
			www-client/google-chrome-unstable
			www-client/chromium
			www-client/google-chrome
			www-client/google-chrome-beta
		)
		www-plugins/adobe-flash
		x11-terms/xterm
		webmail? ( || ( mail-client/thunderbird-bin mail-client/thunderbird ) )
	)"
